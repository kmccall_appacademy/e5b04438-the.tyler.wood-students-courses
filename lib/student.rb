class Student
  attr_accessor :first_name,:last_name,:courses
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []

  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    if @courses.any? { |student_course| student_course.conflicts_with?(course) }
      raise "already enrolled"
    elsif (!course.students.include?(self))
      @courses.push(course)
      course.students.push(self)
    end
  end

  def course_load
    credits = Hash.new(0)
    self.courses.each do |course|
      credits[course.department] += course.credits
    end
    credits
  end

end
